'use strict';

var config = require('../config'),
	dashboardModel = require('../models/dashboardModel.js'),
	request = require('request');

module.exports = (function() {
	function _render(req, res, dashboardKey) {
		var dashboard = config.dashboards[dashboardKey],
			widgets = dashboard.widgets.map(function(tile) {
				return _callAPI(tile);
			});

		Promise.all(
			widgets
		).then(
			function(response) {
				res.render('index', dashboardModel(dashboard, response));
			},
			function(e) {
				console.log('Error');
				console.log(e);
			}
		).catch(function(reason) {
			console.log(reason);
		});
	}

	function _callAPI(tile) {
		return new Promise(function(resolve) {
			var requestOptions = {
				'url': tile.url
			};

			requestOptions = _setIntegrationHeaders(tile.integration, requestOptions);

			request.get(requestOptions, function(error, response, body) {
				console.log(response.statusCode);
				if (!error && response.statusCode === 200) {
					tile.content = JSON.parse(body);

					resolve(tile);
				} else if (error) {
					tile.widget = 'error';
					tile.content = error;

					resolve(tile);
				} else {
					tile.widget = 'error';
					tile.content = error;

					resolve(tile);
				}
			});
		});
	}

	function _setIntegrationHeaders(integration, options) {
		switch (integration) {
			case 'JIRA':
				options.headers = {
					'Authorization': 'Basic ' + config.jira.basicAuthKey
				};
				break;
			case 'GITHUB':
				options.headers = {
					'User-Agent': 'jeroenvdb',
					'Accept': 'application/vnd.github.v3+json'
				};
				break;
			case 'JENKINS':
				options.auth = {
					'user': 'jvandenberghe',
					'pass': config.jenkins.token,
					'sendImmediately': true
				};
				break;
			default:
				options.header = {};

		}

		return options;
	}

	return {
		'render': _render
	};
}());
