'use strict';

var express = require('express'),
	router = express.Router(),
	dashboardController = require('../controllers/dashboardController');

router.get('/dashboard/:dashboardKey', function (req, res) {
	var dashboardKey = req.params.dashboardKey;

	dashboardController.render(req, res, dashboardKey);
});

module.exports = router;
