'use strict';

var hbs = require('hbs'),
	fs = require('fs'),
	path = require('path'),
	glob = require('glob');

glob(__dirname + '/../views/**/*.partial.hbs', {}, function(er, files) {
	files.forEach(function(file) {
		var template = fs.readFileSync(file, 'utf8'),
			templateName = path.basename(file, '.partial.hbs');

		hbs.registerPartial(templateName, template);
	});
});
