'use strict';

var hbs = require('hbs'),
	glob = require('glob'),
	config = require(__dirname + '/../config');

glob(__dirname + '/../widgets/**/index.js', {}, function(er, files) {
	files.forEach(function(file) {
		require(file)(hbs, config);
	});
});
