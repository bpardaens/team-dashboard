'use strict';

var glob = require('glob'),
	config = require(__dirname + '/../config');

glob(__dirname + '/../dashboards/**/index.js', {}, function(er, files) {
	files.forEach(function(file) {
		require(file)(config);
	});
});
