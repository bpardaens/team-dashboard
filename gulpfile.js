var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');

gulp.task('sass', function () {
	gulp.src([
			'./dashboards/sass/*.scss',
			'./widgets/**/*.scss'
		])
		.pipe(sass.sync().on('error', sass.logError))
		.pipe(concat('main.css'))
		.pipe(gulp.dest('./public/css/'));
});

gulp.task('sass:watch', function () {
	gulp.watch('./**/*.scss', ['sass']);
});

gulp.task('build', ['sass']);
