'use strict';

var widgetModel = require('./widgetModel.js'),
	widgetTemplate = require('./widget.hbs');

const WIDGET_NAME = 'jira-sprint-progress';

module.exports = function(hbs, config) {
	hbs.registerPartial(WIDGET_NAME, widgetTemplate);

	config.widgetModelMap[WIDGET_NAME] = widgetModel;
};
