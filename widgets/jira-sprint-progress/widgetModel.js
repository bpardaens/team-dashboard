'use strict';

var Widget;

module.exports = function(widgetData) {
	var widget = new Widget(widgetData);

	widget.setTotalIssuesCount();
	widget.setIssuesPerSegment();
	widget.setFlaggedSegment();
	widget.setPercentagePerSegment();
	widget.removeEmptySegments();

	return widget;
};

Widget = function(data) {
	this.widget = data.widget;
	this.widgetConfig = data.widgetConfig ? data.widgetConfig : {};
	this.title = data.title;
	this.content = data.content;
	this.numberOfIssues = 0;

	this.segments = [
		{
			'title': 'Open',
			'key': 'open',
			'statusName': ['READY FOR ANALYSE', 'READY FOR DEVELOPMENT'],
			'issues': [],
			'numberOfIssues': 0,
			'percentage': 0
		},
		{
			'title': 'In progess',
			'key': 'in-progress',
			'statusName': ['IN PROGRESS', 'IN DEVELOPMENT', 'AWAITING CODE REVIEW', 'IN CODE REVIEW'],
			'issues': [],
			'numberOfIssues': 0,
			'percentage': 0
		},
		{
			'title': 'In test',
			'key': 'in-test',
			'statusName': ['AWAITING TEST', 'IN TEST'],
			'issues': [],
			'numberOfIssues': 0,
			'percentage': 0
		},
		{
			'title': 'Closed',
			'key': 'closed',
			'statusName': ['RESOLVED', 'CLOSED'],
			'issues': [],
			'numberOfIssues': 0,
			'percentage': 0
		}
	];
};

Widget.prototype.setIssuesPerSegment = function() {
	var allIssues = this.content.issues;

	this.segments.forEach(function(segment) {
		segment.issues = allIssues.filter(getIssuesWithStatus, segment);
	});
};

Widget.prototype.setFlaggedSegment = function() {
	var allIssues = this.content.issues,
		flaggedIssues = [];

	flaggedIssues = allIssues.filter(getFlaggedIssues);

	this.segments.unshift({
		'title': 'Flagged',
		'key': 'flagged',
		'statusName': [],
		'issues': flaggedIssues,
		'numberOfIssues': flaggedIssues.length,
		'percentage': 0
	});
};

Widget.prototype.removeEmptySegments = function() {
	this.segments = this.segments.filter(function(segment) {
		return segment.numberOfIssues > 0;
	});
};

Widget.prototype.setPercentagePerSegment = function() {
	var totalNumberOfIssues = this.numberOfIssues;

	this.segments.forEach(function(segment) {
		segment.percentage = segment.numberOfIssues / totalNumberOfIssues * 100;
	});
};

Widget.prototype.setTotalIssuesCount = function() {
	this.numberOfIssues = this.content.issues.length;
};

function getIssuesWithStatus(issue) {
	if (this.statusName.indexOf(issue.fields.status.name.toUpperCase()) > -1 && issue.fields.customfield_10102 === null) {
		this.numberOfIssues = this.numberOfIssues + 1;
		return true;
	} else {
		return false;
	}
}

function getFlaggedIssues(issue) {
	if (issue.fields.customfield_10102 !== null && issue.fields.customfield_10102.length > 0) {
		return true;
	} else {
		return false;
	}
}
