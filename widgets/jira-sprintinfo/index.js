'use strict';
var widgetModel = require('./widgetModel.js'),
	widgetTemplate = require('./widget.hbs');

module.exports = function(hbs, config) {
	hbs.registerPartial('jira-sprintinfo', widgetTemplate);

	config.widgetModelMap['jira-sprintinfo'] = widgetModel;
};
