'use strict';

var moment = require('moment'),
	Widget;

module.exports = function(widgetData) {
	var widget = new Widget(widgetData);

	widget.setFormattedEndDate();

	return widget;
};

Widget = function(data) {
	this.widget = data.widget;
	this.title = data.title;
	this.content = data.content;
};

Widget.prototype.setFormattedEndDate = function() {
	this.formattedEndDate = moment(this.content.endDate).fromNow();
};
