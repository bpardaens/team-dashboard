'use strict';

var Widget;

module.exports = function(widgetData) {
	var widget = new Widget(widgetData);

	widget.setStyleModifier();
	widget.setShowWidget();
	widget.setFlaggedCount();

	return widget;
};

Widget = function(data) {
	this.widget = data.widget;
	this.widgetConfig = data.widgetConfig ? data.widgetConfig : null;
	this.title = data.title;
	this.content = data.content;
	this.styleModifier = 'no-error';
	this.numberOfFlagged = 0;
	this.hasFlagged = false;
	this.showWidget = true;
};

Widget.prototype.setStyleModifier = function() {
	if (this.hasOwnProperty('widgetConfig') && this.widgetConfig.hasOwnProperty('errorLevel')) {
		this.styleModifier = this.content.total >= this.widgetConfig.errorLevel ? 'error' : 'no-error';
	}
};

Widget.prototype.setShowWidget = function() {
	if (this.hasOwnProperty('widgetConfig') && this.widgetConfig.hasOwnProperty('showLevel')) {
		this.showWidget = this.content.total >= this.widgetConfig.showLevel;
	}
};

Widget.prototype.setFlaggedCount = function() {
	var numberOfFlagged = 0;

	this.content.issues.forEach(function(issue) {
		if (issue.fields.customfield_10102 !== null && issue.fields.customfield_10102.length > 0) {
			numberOfFlagged = numberOfFlagged + 1;
		}
	});

	this.numberOfFlagged = numberOfFlagged;
	this.hasFlagged = numberOfFlagged > 0;
};
