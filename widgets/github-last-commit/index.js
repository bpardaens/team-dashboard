'use strict';

var widgetTemplate = require('./widget.hbs');

const WIDGET_NAME = 'github-last-commit';

module.exports = function(hbs) {
	hbs.registerPartial(WIDGET_NAME, widgetTemplate);
};
