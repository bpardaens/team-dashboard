'use strict';

var Widget;

module.exports = function(widgetData) {
	var widget = new Widget(widgetData);

	widget.setErrorMessage();

	return widget;
};

Widget = function(data) {
	this.widget = data.widget;
	this.title = data.title;
	this.content = data.content;
};

Widget.prototype.setErrorMessage = function() {
	var errorMessage = [],
		item;

	for (item in this.content) {
		if (this.content.hasOwnProperty(item)) {
			errorMessage.push(this.content[item]);
		}
	}

	this.errorMessage = errorMessage.join(', ');
};
