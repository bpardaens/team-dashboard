'use strict';

var moment = require('moment'),
	Widget;

module.exports = function(widgetData) {
	var widget = new Widget(widgetData);

	widget.setFormattedBuildTime();
	widget.setStartedBy();

	return widget;
};

Widget = function(data) {
	this.widget = data.widget;
	this.title = data.title;
	this.content = data.content;
};

Widget.prototype.setFormattedBuildTime = function() {
	this.formattedBuildTime = moment(this.content.timestamp, 'x').fromNow();
};

Widget.prototype.setStartedBy = function() {
	this.startedBy = this.content.actions[1].causes[0].userName;
};
