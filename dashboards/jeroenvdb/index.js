'use strict';

var dashboardConfig = {
	'title': 'Jeroenvdb dashboard',
	'widgets': [
		{
			'integration': 'GITHUB',
			'widget': 'github-last-commit',
			'title': 'Last commit for Repo: jeroenvdb.be',
			'url': 'https://api.github.com/repos/jeroenvdb/jeroenvdb.be/commits',
			'content': {}
		}
	]
};

module.exports = function(config) {
	config.dashboards['jeroenvdb'] = dashboardConfig;
};
