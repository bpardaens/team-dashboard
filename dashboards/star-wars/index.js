'use strict';

var dashboardConfig = {
	'title': 'Team Star Wars',
	'widgets': [
		{
			'integration': 'JIRA',
			'widget': 'jira-sprint-progress',
			'title': 'Overall bug progress',
			'url': 'https://atlassian.persgroep.net/jira/rest/api/2/search?jql=filter=18125&expand=fields.status',
			'content': {}
		},
		{
			'integration': 'JIRA',
			'widget': 'jira-sprint-progress',
			'title': 'Sprint progress',
			'url': 'https://atlassian.persgroep.net/jira/rest/api/2/search?jql=filter=18427&expand=fields.status',
			'content': {}
		},
		{
			'integration': 'JIRA',
			'widget': 'jira-filter',
			'widgetConfig': {
				'errorLevel': 1
			},
			'title': 'Issues awaiting code review',
			'url': 'https://atlassian.persgroep.net/jira/rest/api/2/search?jql=filter=18432',
			'content': {}
		},
		{
			'integration': 'JIRA',
			'widget': 'jira-filter',
			'widgetConfig': {
				'errorLevel': 1,
				'showLevel': 1
			},
			'title': 'Open SPOC issues',
			'url': 'https://atlassian.persgroep.net/jira/rest/api/2/search?jql=filter=18453',
			'content': {}
		},
		{
			'integration': 'JENKINS',
			'widget': 'jenkins-jobinfo',
			'title': 'Jenkins: last deploy to DEV',
			'url': 'http://build.persgroep.net/jenkins/job/ADW/job/Continious%20Deployment/view/Deploy%20Feature%20Branch/job/cd_to_dev_ad_web/lastBuild/api/json?pretty=true',
			'content': {}
		},
		{
			'integration': 'JIRA',
			'widget': 'jira-sprintinfo',
			'title': 'End of sprint',
			'url': 'https://atlassian.persgroep.net/jira/rest/agile/1.0/sprint/1764',
			'content': {}
		}
	]
};

module.exports = function(config) {
	config.dashboards['star-wars'] = dashboardConfig;
};
