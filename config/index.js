'use strict';

var env = require('node-env-file');

env('./.env');

module.exports = (function() {
	var config = {
		'widgetModelMap': {},
		'dashboards': {},
		'jira': {
			'basicAuthKey': process.env.JIRA_AUTH_KEY
		},
		'jenkins': {
			'token': process.env.JENKINS_TOKEN
		}
	};

	return config;
}());
