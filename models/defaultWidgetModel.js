'use strict';

module.exports = function(tile) {
	return {
		'widget': tile.widget,
		'title': tile.title,
		'content': tile.content
	};
};
