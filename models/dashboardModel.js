'use strict';

var defaultWidgetModel = require('./defaultWidgetModel.js'),
	config = require('./../config/index.js');

module.exports = function(dashboard, widgets) {
	var model = {
		'title': dashboard.title,
		'widgets': _items(widgets)
	};

	function _items(widgets) {
		return widgets.map(function(tile) {
			if (typeof config.widgetModelMap[tile.widget] === 'function') {
				return config.widgetModelMap[tile.widget](tile);
			} else {
				return defaultWidgetModel(tile);
			}
		});
	}

	return model;
};
