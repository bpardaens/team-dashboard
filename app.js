'use strict';

var express = require('express'),
	app = express(),
	fs = require('fs'),
	glob = require('glob'),
	routes = require('./routes/dashboard.js'),
	hbs = require('hbs'),
	hbsLayouts = require('handlebars-layouts'),
	config = require('./config');

// 	set our view engine to handlebars
app.set('view engine', 'hbs');

//	static files
app.use('/svg', express.static('svg'));

//	static files
app.use('/public', express.static('public'));

//	using our routes
app.use('/', routes);

// register partials
hbs.registerHelper(hbsLayouts(hbs.handlebars));
require(__dirname + '/lib/registerPartials.js');

require(__dirname + '/lib/loadDashboards.js');
require(__dirname + '/lib/loadWidgets.js');

// start our server
var port = process.env.PORT || 3000
var server = app.listen(port, function() {
	var host = server.address().address;
	var port = server.address().port;
	console.log('Server is running at http://%s/%s', host, port);
});
